export class AnimalFetchService {
  type: string;

  constructor() {
  }

  static async fetchAnimal(type: string) {
    let url = '';
    if (type === 'dog') {
      url = 'https://dog.ceo/api/breeds/image/random';
    } else {
      url = 'https://api.thecatapi.com/v1/images/search';
    }
    try {
      const response = await fetch(url, {
        method: 'GET',
      });
      const animal = await response.json();
      return animal;
    } catch (error) {
      console.error(error);
    }
  }

  static async setImage(pet: any) {
    const response = await AnimalFetchService.fetchAnimal(pet.type);
    if (pet.type === 'dog') {
      pet.image = response.message;
    } else {
      pet.image = response[0].url
    }
  }
}
