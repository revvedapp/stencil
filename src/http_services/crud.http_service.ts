import {RouteService} from '../services/route.service';
import {SessionStorageService} from '../services/session-storage.service';
import {SessionService} from '../services/session.service';

export class CrudHttpService {
  url: string;
  token: string;
  constructor(apiUrl: string) {
    this.url = `http://localhost:3000/${apiUrl}`
  }

  headers(additional: any = {}): any {
    const token = SessionService.get().token;
    const required = {Authorization: `Token token=${token}`, 'Session-Id': token};
    return {...required, ...additional};
  }

  async query(params: any, cache: boolean = false) {
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    const fullUrl = `${this.url}?${queryString}`;
    return await this.getRequest(fullUrl, cache);
  }

  async findBy(params: any, cache: boolean = false) {
    const id = params.id;
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    const fullUrl = `${this.url}/${id}?${queryString}`;
    return await this.getRequest(fullUrl, cache);
  }

  async put(payload: any) {
    if (payload.id === null || payload.id === undefined) { return; }
    const fullUrl = `${this.url}/${payload.id}`;
    const response = await fetch(fullUrl, {
      method: 'PUT',
      headers: {...this.headers(), 'Content-Type': 'application/json'},
      mode: 'cors',
      redirect: 'follow', // manual, *follow, error
      body: JSON.stringify(payload),

    });
    return await response.json();
  }

  async delete(params: any) {
    if (params.id === null || params.id === undefined) { return; }
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');

    const fullUrl = `${this.url}/${params.id}?${queryString}`;
    const response = await fetch(fullUrl, {
      method: 'DELETE',
      headers: this.headers(),
      mode: 'cors',
    });
    return await response;
  }

  async post(payload: any, params?: any) {
    return await this.postRequest(payload, params);
  }

  async upsert(params?: any) {
    return this.newPost('upsert', params);
  }

  async newPost(action: string, payload?: any) {
    return this.postRequest(payload, {action: action}, 'new');
  }

  async postRequest(payload: any, params?: any, subRoute?: string) {
    let fullUrl = `${this.url}`;
    if (subRoute !== undefined) {
      fullUrl = `${fullUrl}/new`;
    }
    const additionalHeaders = {};
    if (params !== undefined) {
      if (!params.organization_id) {
        additionalHeaders['X-Organization-Id'] = RouteService.params().organization_id;
      }
      const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
      fullUrl = `${fullUrl}?${queryString}`;
    }
    try {
      const response = await fetch(fullUrl, {
        method: 'POST',
        redirect: 'follow', // manual, *follow, error

        headers: {...this.headers(), 'Content-Type': 'application/json', ...additionalHeaders},
        mode: 'cors',
        body: JSON.stringify(payload),
      });
      if (response.status === 500) {
        if (location.hash.includes('errors')) { return; }
        location.hash = `/errors/server`;
        throw new Error('Internal Server Error');
      } else if (response.status === 503) {
        if (location.hash.includes('errors')) { return; }
        location.hash = `/errors/server`;
        throw new Error('Build in progress');
      } else {
        const data = await response.json();
        return data;
      }
    } catch (error) {
      console.error(error);
      if (location.hash.includes('errors')) { return; }
    }
  }

  async find(id: string, cache: boolean = false) {
    const fullUrl = `${this.url}/${id}`;
    return await this.getRequest(fullUrl, cache);
  }

  async getRequest(fullUrl, cache: boolean = false) {
    const cachedData = SessionStorageService.get(fullUrl);
    if (cachedData && cache) {
      return cachedData;
    }
    try {
      const response = await fetch(fullUrl, {
        method: 'GET',
        headers: this.headers(),
        mode: 'cors',
      });
      if (response.status === 204) {
        return [];
      } else if (response.status === 404) {
        return undefined;
      } else {
        const data = await response.json();
        if (cache) {
          SessionStorageService.set(fullUrl, data);
        }
        return data;
      }
    } catch (error) {
      console.error(error);
      if (location.hash.includes('errors')) { return; }
      location.hash = `/errors/server`;
    }
  }
}
