import {CrudHttpService} from './crud.http_service';

export class PetHttpService extends CrudHttpService {
  constructor() {
    super('pets');
  }
}
