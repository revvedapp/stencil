import {CrudHttpService} from './crud.http_service';

export class ShelterHttpService extends CrudHttpService {
  constructor() {
    super('shelters');
  }
}
