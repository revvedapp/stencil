import {Component, Element, Event, EventEmitter, Prop, h} from '@stencil/core';

@Component({
  tag: 'pet-filter-options',
})
export class TaskFilterOptions {
  @Element() el: Element;
  @Prop({mutable: true}) selected = 'all';
  @Prop() options: {value: string, display: string}[];
  @Prop() name: string;
  @Event() filterSelected: EventEmitter;
  dismiss(data: any) {
    this.filterSelected.emit({name: this.name, data: data});
    (this.el.closest('ion-popover') as any).dismiss();
  }

  renderOptions() {
    return this.options.map((option) => {
      return (
        <ion-item disabled={this.selected === option.value} detail={false} button={true} onClick={ () => this.dismiss(option)}>
          <ion-label>
            {option.display}
          </ion-label>
        </ion-item>
      );
    });
  }

  render() {
    return [
      <ion-list lines='none' >
        {this.renderOptions()}
      </ion-list>
    ];
  }
}
