import {Component, h, Prop, Element, Event, EventEmitter, State} from '@stencil/core';
import {PlatformService} from "../../../services/platform.service";

@Component({
  tag: 'adoption-form',
})
export class AdoptionForm {

  @State() ownerToggle = false;
  @Prop() pet;
  @Element() el: Element;
  @Event() modalDismissed: EventEmitter;
  @State() submitted = false;
  firstName: HTMLIonInputElement;
  lastName: HTMLIonInputElement;
  ownedPets: HTMLIonTextareaElement;

  dismiss() {
    this.modalDismissed.emit();
    (this.el.closest('ion-modal') as any).dismiss();
  }

  async submitForm() {
    // await new AdoptionFormHttpService.put({
    //   petID: this.pet.id,
    //   firstName: this.firstName,
    //   lastName: this.lastName
    // });
    this.submitted = true;
    this.dismiss();
  }

  togglePetOwner() {
    this.ownerToggle = !this.ownerToggle;
  }

  renderNoMatch() {
    return [
      <ion-card>
        <ion-item>
          Sorry, no pets appear to match your search.
        </ion-item>
      </ion-card>
    ]
  }

  renderSubmitted() {
    return [
      <ion-card>
        <ion-item>
          Thank you for your submission! We'll be reaching out soon.
        </ion-item>
      </ion-card>
    ]
  }

  renderAdditionalPets() {
    if (!this.ownerToggle) { return; }
    return [
      <ion-item>
        <ion-label position='floating'>
          List pets:
        </ion-label>
        <ion-textarea autoGrow={true} ref={(element) => { this.ownedPets = element; }}/>
      </ion-item>
    ]
  }

  renderCheckmarkIcon() {
    if (this.ownerToggle) {
      return [
        <ion-icon name='checkmark-circle-outline' />
      ]
    } else {
      return [
        <ion-icon name='ellipse-outline' />
      ]
    }
  }

  renderHeaderButton() {
    if (PlatformService.desktop()) { return; }
    return [
      <ion-buttons slot='start'>
        <ion-button
          slot='icon-only'
          onClick={() => this.dismiss()}>
          <ion-icon name='close'/>
        </ion-button>
      </ion-buttons>
    ]
  }

  renderAdoptionForm() {
    return [
      <ion-list>
        <ion-item>
          <ion-label position='floating'>
            First Name
          </ion-label>
          <ion-input ref={(element) => { this.firstName = element; }}/>
        </ion-item>
        <ion-item>
          <ion-label position='floating'>
            Last Name
          </ion-label>
          <ion-input ref={(element) => { this.lastName = element; }}/>
        </ion-item>
        <ion-item button={true} detail={false} onClick={ () => this.togglePetOwner() }>
          <ion-label>Do you already own a pet?</ion-label>
          {this.renderCheckmarkIcon()}
        </ion-item>
        {this.renderAdditionalPets()}
        <ion-item lines='none'>
          <ion-button color='dark' style={{width: '95%', margin: 'auto'}} onClick={() => this.submitForm()}>
            Submit Application
          </ion-button>
        </ion-item>
      </ion-list>
    ]
  }

  render() {
    if (this.submitted) { return this.renderSubmitted()}
    if (Object.keys(this.pet).length == 0) { return this.renderNoMatch()}
    return [
      <ion-header>
        <ion-toolbar >
          {this.renderHeaderButton()}
          <ion-title>Adopt {this.pet.name}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <div style={{'overflow-y': 'scroll', height: 'calc(100vh - 56px)', paddingTop: '8px', backgroundColor: '#F4F4F4'}}>
        {this.renderAdoptionForm()}
      </div>
    ];
  }
}
