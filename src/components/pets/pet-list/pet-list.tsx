import {Component, h, Listen, Prop, State} from '@stencil/core';
import { PETS } from '../../../faked-data/pets/fake-pets';
import { SHELTERS } from "../../../faked-data/shelters/fake-shelters";
import {modalController, popoverController} from "@ionic/core";
import {PlatformService} from "../../../services/platform.service";
import _ from 'underscore';
import {SearchService} from "../../../services/search-service";

@Component({
  tag: 'pet-list',
  styleUrl: 'pet-list.css'
})
export class PetList {

  @State() filteredPets: any;
  @State() filterType: {value: string, display: string} = {value: 'all', display: 'All'};
  @State() openModalPetId = null;
  @Prop() shelterId;
  filterOptions: {value: string, display: string}[];
  needle: any;
  shelter: any;
  // pets: any;


  async componentWillLoad() {
    // this.shelter = await newShelterHttpService.findBy(id: this.shelterId)
    // this.pets = await new PetsHttpService.where(shelterId: this.shelterId)
    this.shelter = SHELTERS.find(shelter => shelter.id === parseInt(this.shelterId));
    this.filterOptions = [
      {value: 'all', display: 'All'},
      {value: 'cats', display: 'Cats'},
      {value: 'dogs', display: 'Dogs'},
    ];
    this.filteredPets = _.sortBy(PETS, (pet) => `${pet.type} ${pet.name}`);
    if (!PlatformService.desktop()) { return; }
    this.renderPetModal(this.filteredPets[0])
  }

  async presentFilterOptions(event) {
    const popover = await popoverController.create({
      component: 'pet-filter-options',
      event: event,
      showBackdrop: false,
      translucent: true,
      componentProps: {
        selected: this.filterType.value,
        options: this.filterOptions,
        name: 'petFilter',
      }
    });
    return await popover.present();
  }

  @Listen('modalDismissed', {target: 'document'})
  modalDismissed() {
    this.openModalPetId = null;
  }

  @Listen('filterSelected', {target: 'body'})
  updateFilter(event) {
    if (event.type === 'filterSelected') {
      this.filterType = event.detail.data;
    } else if (event.type === 'input' || event.type === 'ionClear') {
      this.needle = event.target.value;
    }
    const copyPETS = [...PETS];
    let filtered = [];
    if (this.filterType.value === 'all') {
      filtered = copyPETS;
    } else if (this.filterType.value === 'cats') {
      filtered = _.where(copyPETS, {type: 'cat'})
    } else if (this.filterType.value === 'dogs') {
      filtered = _.where(copyPETS, {type: 'dog'})
    }
    const results = SearchService.search(filtered, this.needle, ['name', 'type', 'breed', 'sex']);
    this.filteredPets = _.sortBy([...results], (pet: any) => `${pet.type} ${pet.name}`);
    if (this.filteredPets.length !== 0 && PlatformService.desktop()) {
      this.openModalPetId = this.filteredPets[0].id
    }
  }

  @Listen('ionClear')
  search(event) {
    if (event.type === 'ionClear') {
      event.target.value = '';
    }
    this.updateFilter(event);
  }

  renderPetPreviewPane() {
    if (!PlatformService.desktop()) { return; }
    let pet = this.filteredPets.find(pet => { return pet.id === this.openModalPetId});
    if (pet === undefined) {
      pet = {}
    }
    return [
      <ion-col style={{ overflowY: 'scroll', height: '100%'}} class='ion-no-padding' >
        <adoption-form pet={pet} />
      </ion-col>
    ];
  }

  dismissAllModals() {
    this.openModalPetId = null;
    document.querySelectorAll('ion-modal').forEach((oldModal) => oldModal.dismiss() );
  }

  async renderPetModal(pet) {
    if (this.openModalPetId === pet.id) { return; }
    if (PlatformService.desktop()) { this.openModalPetId = pet.id; return; }
    this.dismissAllModals();
    const modal = await modalController.create({
      cssClass: PlatformService.desktop() ? 'split-pane' : '',
      component: 'adoption-form',
      componentProps: {pet: pet}
    });
    this.openModalPetId = pet.id;
    await modal.present()
  }

  renderPets() {
    return this.filteredPets.map((pet) => {
      return [
        <ion-card button={true} onClick={() => this.renderPetModal(pet)}>
          <img src={pet.image} style={{width: '100%'}} alt={pet.name}/>
          <ion-card-header>
            {pet.name}
          </ion-card-header>
          <ion-card-content>
            <ion-list lines='none' >
              <ion-item>
                Breed: {pet.breed}
              </ion-item>
              <ion-item>
                Age: {pet.age}
              </ion-item>
            </ion-list>
          </ion-card-content>
        </ion-card>
      ]
    })
  }

  render() {
    return [
      <ion-row style={{height: '100%'}}>
        <ion-col style={{height: '100%', borderRight: '2px solid lightgray', background: 'white'}} class='ion-no-padding'>
          <ion-header>
            <ion-toolbar>
              <ion-buttons slot='start'>
                <ion-button
                  slot='icon-only'
                  onClick={() => this.dismissAllModals()}
                  href='/'
                >
                  <ion-icon name='arrow-back'/>
                </ion-button>
              </ion-buttons>
              <ion-title>{this.shelter.name}</ion-title>
              <ion-button fill='clear' slot='end' onClick={(event) => this.presentFilterOptions(event)}>
                <ion-icon name='filter-outline' color='dark'/>
              </ion-button>
            </ion-toolbar>
            <ion-toolbar>
              <ion-searchbar onInput={(event) => this.search(event)}/>
            </ion-toolbar>
          </ion-header>
          <ion-content style={{height: 'calc(100vh - 112px'}}>
            {this.renderPets()}
          </ion-content>
        </ion-col>
        {this.renderPetPreviewPane()}
      </ion-row>
    ];
  }
}

