import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  render() {
    return (
      <ion-app>
        <ion-router useHash={true}>
          <ion-route url="/" component="shelter-list" />
          <ion-route url="/shelters/:shelterId" component="pet-list" />
          <ion-route url="/errors/server" component="errors-server" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
