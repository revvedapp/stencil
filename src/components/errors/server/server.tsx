import {Component, h} from '@stencil/core';
import {modalController} from '@ionic/core';
import {RouteService} from '../../../services/route.service';

@Component({
  tag: 'errors-server'
})
export class ServerError {

  renderCloseButton() {
    if (RouteService.path().includes('error')) {
      return [
        <ion-header>
          <ion-toolbar >
            <ion-buttons slot='start'>
              <ion-button slot='icon-only' href="#/">
                <ion-icon name='arrow-back' />
              </ion-button>
            </ion-buttons>
            <ion-title>Server Error</ion-title>
          </ion-toolbar>
        </ion-header>
      ];
    }

    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-button slot='icon-only' onClick={  modalController.dismiss }>
              <ion-icon name='close'/>
            </ion-button>
            <ion-menu-button/>
          </ion-buttons>
          <ion-title>Server Error</ion-title>
        </ion-toolbar>
      </ion-header>
    ];
  }

  render() {
    return [
      this.renderCloseButton(),
      <ion-content>
        <ion-grid>
          <ion-row>
            <ion-col align-self-center={true} style={{textAlign: 'center'}}>
              <img src='../../../assets/images/500.jpg' />
              <h2>OOPS</h2>
              <p><ion-note>Something went wrong.</ion-note></p>
              <ion-button
                fill='outline'
                shape='round'
                href={`/`} >
                Go Back
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
