import {Component, h} from '@stencil/core';
import {RouteService} from '../../../services/route.service';
import {modalController} from '@ionic/core';

@Component({
  tag: 'errors-not-found'
})
export class NotFound {

  renderMessage() {
    if (RouteService.path().includes('404')) {
      return [
        <h2>404</h2>,
        <p><ion-note>Nothing Here. Keep cruising along.</ion-note></p>
      ];
    } else {
      return [
        <h2>Nothing Here</h2>,
        <p><ion-note>Let's try something else.</ion-note></p>,
        <ion-button
          fill='outline'
          shape='round'
          href={`#/`} >
          Go Back
        </ion-button>,
      ];
    }
  }

  goBack() {
    window.history.back();
  }

  renderCloseButton() {
    if (RouteService.path().includes('404')) { return; }

    if (RouteService.path().includes('error')) {
      return [
        <ion-header>
          <ion-toolbar >
            <ion-buttons slot='start' >
              <ion-button slot='icon-only' onClick={ this.goBack }>
                <ion-icon name='arrow-back' />
              </ion-button>
            </ion-buttons>
            <ion-title>Not Found</ion-title>
          </ion-toolbar>
        </ion-header>,
      ];
    }

    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-button slot='icon-only' onClick={ modalController.dismiss }>
              <ion-icon name='close'/>
            </ion-button>
          </ion-buttons>
          <ion-title>Not Found</ion-title>
        </ion-toolbar>
      </ion-header>,
    ];
  }

  render() {
    return [
      this.renderCloseButton(),
      <ion-content>
        <ion-grid>
          <ion-row>
            <ion-col align-self-center={true} style={{textAlign: 'center'}}>
              <img src='../../../assets/images/404.jpg' />
              {this.renderMessage()}
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
