import { Component, h } from '@stencil/core';
import { SHELTERS } from '../../faked-data/shelters/fake-shelters';
import {PlatformService} from "../../services/platform.service";

@Component({
  tag: 'shelter-list'
})
export class ShelterList {

  renderDesktopShelters() {
    return SHELTERS.map((shelter) => {
      return [
        <ion-col size='6'>
          <ion-card href={`/shelters/${shelter.id}`}>
            <img src={shelter.image} style={{width: '100%', maxHeight: '428px'}} alt='image of shelter'/>
            <ion-card-header>
              {shelter.name}
            </ion-card-header>
            <ion-card-content>
              {shelter.location}
            </ion-card-content>
          </ion-card>
        </ion-col>
      ]
    })
  }

  renderShelters() {
    if (PlatformService.desktop()) {
      return [
        <ion-row class='ion-align-items-stretch'>
          {this.renderDesktopShelters()}
        </ion-row>
      ]
    } else {
        return SHELTERS.map((shelter) => {
          return [
            <ion-card href={`/shelters/${shelter.id}`}>
              <img src={shelter.image} style={{width: '100%'}} alt='image of shelter'/>
              <ion-card-header>
                {shelter.name}
              </ion-card-header>
              <ion-card-content>
                {shelter.location}
              </ion-card-content>
            </ion-card>
          ]
        })
      }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-button
              slot='icon-only'
              href='https://heptagon-swan-aafk.squarespace.com/'
            >
              <ion-icon color='dark' name='home'/>
            </ion-button>
          </ion-buttons>
          <ion-title>revved pets</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
      {this.renderShelters()}
      </ion-content>
    ];
  }
}
