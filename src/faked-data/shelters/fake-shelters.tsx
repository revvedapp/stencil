export const SHELTERS =
  [
    {name: "Arlington Animal Shelter", location: '1000 Wilson Blvd, Arlington, VA', image: 'assets/images/shelter1.jpg', id: 1},
    {name: "Alachua Animal Shelter", location: '2100 34th St, Gainesville, FL', image: 'assets/images/shelter2.jpeg', id: 2},
    {name: "Adopt-a-pet", location: '800 Pets Ave, Austin, TX', image: 'assets/images/shelter3.jpeg', id: 3},
    {name: "World's Best Doggos", location: '102 Dog Place, Orlando, FL', image: 'assets/images/shelter4.jpeg', id: 4},
    {name: "Kittens 4 Sale", location: '956 Feline Drive, Tampa, FL', image: 'assets/images/shelter5.jpeg', id: 5},
  ];
