export const PETS =
  [
    {name: "Harriet", breed: 'Domestic Shorthair', age: 5, id: 1, image: 'assets/images/harriet.jpg', type: 'cat', sex: 'female'},
    {name: "Lassie", breed: 'Collie', age: 6, id: 2, image: 'assets/images/lassie.jpg', type: 'dog', sex: 'female'},
    {name: "Shiloh", breed: 'Beagle', age: 8, id: 3, image: 'assets/images/shiloh.jpg', type: 'dog', sex: 'male'},
    {name: "Bud", breed: 'Golden Retriever', age: 4, id: 4, image: 'assets/images/bud.png', type: 'dog', sex: 'male'},
    {name: "Maru", breed: 'Scottish Fold', age: 12, id: 5, image: 'assets/images/maru.jpg', type: 'cat', sex: 'male'},
    {name: "JoJo", breed: 'German Shepard', age: 4, id: 6, image: 'assets/images/jojo.jpg', type: 'dog', sex: 'female'},
  ];
